# DevOps Resume
Project to build a webpage on AWS that is built completely using DevOps tooling.  Goal tools:
- [ ] Terraform
- [ ] Docker
- [ ] AWS
- [ ] JavaScript Flavour
- [ ] Python
- [ ] Bash Scripting
- [ ] Something CI (CircleCI? Jenkins?)
